/**
 * @description       : 
 * @author            : Caballero, Jose Manuel
 * @group             : 
 * @last modified on  : 07-06-2020
 * @last modified by  : Caballero, Jose Manuel
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   07-04-2020   Caballero, Jose Manuel   Initial Version
**/
public with sharing class ContactController {  
    @AuraEnabled(cacheable = true)  
    public static List<Contact> getContactsList(Integer pagenumber, Integer numberOfRecords, Integer pageSize, String searchString) {  
      String searchKey = '%' + searchString + '%';  
      String query = 'select Name, Title, Department, Email, Phone from Contact ';  
      if (searchString != null && searchString != '') {  
        query += ' where Name like \'%' + searchString + '%\' ';  
      }  
      query += ' limit ' + pageSize + ' offset ' + (pageSize * (pagenumber - 1));  
      return Database.query(query);  
    }  
    @AuraEnabled(cacheable = true)  
    public static Integer getContactsCount(String searchString) {  
      String query = 'select count() from Contact ';  
      if (searchString != null && searchString != '') {  
        query += ' where name like \'%' + searchString + '%\' ';  
      }  
      return Database.countQuery(query);  
    }  
  }