/**
 * @description       : 
 * @author            : Caballero, Jose Manuel
 * @group             : 
 * @last modified on  : 07-07-2020
 * @last modified by  : Caballero, Jose Manuel
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   07-05-2020   Caballero, Jose Manuel   Initial Version
**/
@RestResource(urlMapping='/Contacts/*')
global with sharing class SyncContactsWS {
    
    @HttpPut
    global static Integer syncContacts () {

        RestRequest request = RestContext.request;
        String reqBody = request.requestBody.toString();
        List<Contact> contacts =(List<Contact>)JSON.deserialize(reqBody, List<Contact>.class);
        for (Contact c : contacts)
        {
            c.Id = null;
        }
        
        Schema.SObjectField externalIdField = Contact.Fields.External_ID_1__c;
        List<Database.UpsertResult> upsertLog= new List<Database.UpsertResult>();
        Boolean upsertKO = false;

        for(Database.UpsertResult upsertResult: Database.upsert(contacts,externalIdField, false)){
            upsertLog.add(upsertResult);
            System.debug('CONTACT: '+upsertResult.isSuccess()+' _ '+upsertResult.getId());
            if(!upsertResult.isSuccess())
                upsertKO = true;
        }

        RestResponse response = RestContext.response;
        response.responseBody = Blob.valueOf(JSON.serialize(upsertLog));
        if(upsertKO)
        {   
            response.statusCode = 400;
        }else 
        {
            response.statusCode = 200;
        }
        Integer statusCode = response.statusCode;
        return statusCode;
        
    }

}