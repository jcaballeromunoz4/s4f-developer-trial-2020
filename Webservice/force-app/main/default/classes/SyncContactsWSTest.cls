/**
 * @description       : 
 * @author            : Caballero, Jose Manuel
 * @group             : 
 * @last modified on  : 07-07-2020
 * @last modified by  : Caballero, Jose Manuel
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   07-06-2020   Caballero, Jose Manuel   Initial Version
**/
@isTest
public class SyncContactsWSTest {

    @testSetup
    static void createData() {

        List<Contact> cListUpsert = TestDataFactory.createSObjectList('Contact', new Map<String,Object>{
            'External_ID_1__c' => 'test{!index}'
          },10);


    }

    @isTest static void SyncContactsWSTest() {
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        List<Contact> cList = [SELECT FirstName,LastName, Title, Department, Phone, MobilePhone, Fax, Email, OtherCountry, OtherCity, OtherPostalCode, OtherStreet, OtherState, External_ID_1__c, Birthdate FROM Contact];
        String requestBody = JSON.serialize(cList);

        req.requestBody = Blob.valueOf(requestBody);
        req.requestURI = '/services/apexrest/Contacts/';  
        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;
        
        
        Test.startTest();
        Integer statusCode = SyncContactsWS.syncContacts();
        System.assertEquals(200, statusCode);
        Test.stopTest();
    }

    @isTest static void SyncContactsWSTestKO() {
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        List<Contact> cList = [SELECT FirstName,LastName, Title, Department, Phone, MobilePhone, Fax, Email, OtherCountry, OtherCity, OtherPostalCode, OtherStreet, OtherState, External_ID_1__c, Birthdate FROM Contact];
        for (Contact c: cList)
        {
            c.External_ID_1__c = null;
        }
        String requestBody = JSON.serialize(cList);

        req.requestBody = Blob.valueOf(requestBody);
        req.requestURI = '/services/apexrest/Contacts/';  
        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;
        
        
        Test.startTest();
        Integer statusCode = SyncContactsWS.syncContacts();
        System.assertEquals(400, statusCode);
        Test.stopTest();

    }
}
