/**
 * @description       : 
 * @author            : Caballero, Jose Manuel
 * @group             : 
 * @last modified on  : 07-06-2020
 * @last modified by  : Caballero, Jose Manuel
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   07-06-2020   Caballero, Jose Manuel   Initial Version
**/

@isTest (SeeAllData = False)

public class ContactControllerTest {

    @testSetup
    static void createData() {

        List<Contact> conList = TestDataFactory.createSObjectList('Contact',10);
        update conList;
        Contact con = (Contact)TestDataFactory.createSObject('Contact', new Map<String,Object>{
            'FirstName' => 'Doe',
            'LastName' => 'John'});
        update con;
        

    }

    @isTest
    static void testGetContactsByName(){
        Test.startTest();
        Integer pagenumber = 1;
        Integer numberOfRecords = 20;
        Integer pageSize = 20;
        String searchString = '';
        List<Contact> queryResult = ContactController.getContactsList(pagenumber, numberOfRecords, pageSize, searchString);
        System.assertEquals(11, queryResult.size());
        Test.stopTest();
    }

    @isTest
    static void testGetContactsByNameJohnDoe(){
        Test.startTest();
        Integer pagenumber = 1;
        Integer numberOfRecords = 20;
        Integer pageSize = 20;
        String searchString = 'John';
        List<Contact> queryResult = ContactController.getContactsList(pagenumber, numberOfRecords, pageSize, searchString);
        System.assertEquals(1, queryResult.size());
        Test.stopTest();
    }
    @isTest
    static void testGetContactsCount(){
        Test.startTest();
        String searchString = '';
        Integer queryResult = ContactController.getContactsCount(searchString);
        System.assertEquals(11, queryResult);
        Test.stopTest();
    }

    @isTest
    static void testGetContactsCountJohnDoe(){
        Test.startTest();
        String searchString = 'John';
        Integer queryResult = ContactController.getContactsCount(searchString);
        System.assertEquals(1, queryResult);
        Test.stopTest();
    }
}