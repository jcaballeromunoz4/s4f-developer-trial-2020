import { LightningElement, track, api } from 'lwc';  
import getContactsList from '@salesforce/apex/ContactController.getContactsList';  
import getContactsCount from '@salesforce/apex/ContactController.getContactsCount';  
export default class ContactList extends LightningElement {  
  @track contacts;  
  @track error;  
  @api currentpage;  
  @api pagesize;  
  @track searchKey;  
  totalpages;  
  localCurrentPage = null;  
  isSearchChangeExecuted = false;  
  
  handleKeyChange(event) {  
    if (this.searchKey !== event.target.value) {  
      this.isSearchChangeExecuted = false;  
      this.searchKey = event.target.value;  
      this.currentpage = 1;  
    }  
  }
  
  handlePageSizeChange(event) {  
    if (this.pagesize !== event.target.value) {  
      this.isSearchChangeExecuted = false;  
      this.pagesize = event.target.value;  
      this.currentpage = 1;  
    }  
  } 
  

  renderedCallback() {  
    // This line added to avoid duplicate/multiple executions of this code.  
    if (this.isSearchChangeExecuted && (this.localCurrentPage === this.currentpage)) {  
      return;  
    }  
    this.isSearchChangeExecuted = true;  
    this.localCurrentPage = this.currentpage;  
    getContactsCount({ searchString: this.searchKey })  
      .then(recordsCount => {  
        this.totalrecords = recordsCount;  
        if (recordsCount !== 0 && !isNaN(recordsCount)) {  
          this.totalpages = Math.ceil(recordsCount / this.pagesize);  
          getContactsList({ pagenumber: this.currentpage, numberOfRecords: recordsCount, pageSize: this.pagesize, searchString: this.searchKey })  
            .then(contactList => {  
              this.contacts = contactList;  
              this.error = undefined;  
            })  
            .catch(error => {  
              this.error = error;  
              this.contacts = undefined;  
            });  
        } else {  
          this.contacts = [];  
          this.totalpages = 1;  
          this.totalrecords = 0;  
        }  
        const event = new CustomEvent('recordsload', {  
          detail: recordsCount  
        });  
        this.dispatchEvent(event);  
      })  
      .catch(error => {  
        this.error = error;  
        this.totalrecords = undefined;  
      });  
  }  
}