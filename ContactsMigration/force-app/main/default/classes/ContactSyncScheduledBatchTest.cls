/**
 * @description       : 
 * @author            : Caballero, Jose Manuel
 * @group             : 
 * @last modified on  : 07-07-2020
 * @last modified by  : Caballero, Jose Manuel
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   07-06-2020   Caballero, Jose Manuel   Initial Version
**/
@isTest

public class ContactSyncScheduledBatchTest {

    @testSetup
    static void createData() {

        List<Contact> contactList = TestDataFactory.createSObjectList('Contact', new Map<String,Object>{
          'SyncStatus__c' => 'Not Synced',
          'External_ID_1__c' => 'EXTERNAL_ID_TEST_{!index}'
        },2, true);

    }

    @isTest
    static void testBatchhOK(){
        Test.setMock(HttpCalloutMock.class, new ContactSyncHttpMock(200));
        Test.startTest();
        ContactSyncScheduledBatch sB = new ContactSyncScheduledBatch();
        String jobID = Database.executeBatch(sB);
        Test.stopTest();
        List<Contact> cList = [SELECT Id FROM Contact where SyncStatus__c = 'Synced'];
        System.assertEquals(2,cList.size());
    }

    
    @isTest
    static void testBatchKO(){
        Test.setMock(HttpCalloutMock.class, new ContactSyncHttpMock(400));
        Test.startTest();
        ContactSyncScheduledBatch sB = new ContactSyncScheduledBatch();
        String jobID = Database.executeBatch(sB);Database.executeBatch(sB);
        Test.stopTest();
        List<Contact> cList = [SELECT Id FROM Contact where SyncStatus__c = 'Error During Sync'];
        System.assertEquals(2,cList.size());
    }

	public class ContactSyncHttpMock implements HttpCalloutMock {

		public Integer statusCode;

		public ContactSyncHttpMock(Integer statusCode){
			this.statusCode = statusCode;
		}

		public HTTPResponse respond(HTTPRequest req) {
			
			// Create a fake response
			HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"example":"test"}');
			res.setStatusCode(statusCode);
			return res;
		}
	}
   
}