/**
 * @description       : 
 * @author            : Caballero, Jose Manuel
 * @group             : 
 * @last modified on  : 07-07-2020
 * @last modified by  : Caballero, Jose Manuel
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   07-05-2020   Caballero, Jose Manuel   Initial Version
**/
global class ContactSyncScheduledBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    
	private static String EXTERNAL_ID_FIELD_NAME = 'External_ID_1__c';
    private static String NAMED_CREDENTIALS = 'callout:Org_B';
    private static Integer BATCHSIZE = 100;
    
    //Batchable
    global Database.QueryLocator start(Database.BatchableContext context)
    {
        String query = 'SELECT FirstName,LastName, Title, Department, Phone, MobilePhone, Fax, Email, OtherCountry, OtherCity, OtherPostalCode, OtherStreet, OtherState, External_ID_1__c, Birthdate, SyncStatus__c FROM Contact where External_ID_1__C != null';
        return Database.getQueryLocator(query);
    }
 
    global void execute(Database.BatchableContext context, List<Contact> scope)
    {    
        String endPoint = NAMED_CREDENTIALS + '/services/apexrest/Contacts/';
        //complete scope is serialized
        String requestBody = JSON.serialize(scope);
		
        //do the call for the complete 200 batch
        HttpRequest request = new HttpRequest();
        request.setMethod('PUT');
        request.setEndpoint(endPoint);
        request.setHeader('Content-Type', 'application/json');
        request.setBody(requestBody);
        Http http = new Http();
        HTTPResponse response = http.send(request);
        
        Integer statusCode = response.getStatusCode();
		
        //check status of the response 
        if (statusCode != 200) {
            System.debug('The status code returned was not expected: ' +
            statusCode);
            for (Contact c : scope)
            {
                //update records belonging to the batch that failed
                c.SyncStatus__c = 'Error During Sync';
            }
            update scope;
            System.debug('The status code returned was not expected: ' +
            statusCode);
        } else {
            for (Contact c : scope)
            {
                //if the batch was ok then update all records with Synced status
                c.SyncStatus__c = 'Synced';
            }
            update scope;
            System.debug('Operation was fine');
        }
        System.debug(response.getBody());
    }

    global void finish(Database.BatchableContext context)
    {
        //Method can be scheduled daily, using this code, or it can be changed into Schedulable implementation to be scheduled in periods of time.
        //ContactSyncScheduledBatch sB = new ContactSyncScheduledBatch();
       	//System.scheduleBatch(sB, 'Contact Sync Job', 1440);
    }
}