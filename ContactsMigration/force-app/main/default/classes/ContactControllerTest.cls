/**
 * @description       : 
 * @author            : Caballero, Jose Manuel
 * @group             : 
 * @last modified on  : 07-06-2020
 * @last modified by  : Caballero, Jose Manuel
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   07-06-2020   Caballero, Jose Manuel   Initial Version
**/

@isTest (SeeAllData = False)

public class ContactControllerTest {

    @testSetup
    static void createData() {

        List<Contact> conList = TestDataFactory.createSObjectList('Contact',10);
        Contact con = (Contact)TestDataFactory.createSObject('Contact', new Map<String,Object>{
            'FirstName' => 'Doe',
            'LastName' => 'John'},true);
        

    }

    @isTest
    static void testGetContactsByName(){
        
        Integer pagenumber = 1;
        Integer numberOfRecords = 20;
        Integer pageSize = 20;
        String searchString = '';
        Test.startTest();
        List<Contact> queryResult = ContactController.getContactsList(pagenumber, numberOfRecords, pageSize, searchString);
        Test.stopTest();
    }

    @isTest
    static void testGetContactsByNameJohnDoe(){
        Test.startTest();
        Integer pagenumber = 1;
        Integer numberOfRecords = 20;
        Integer pageSize = 20;
        String searchString = 'John';
        List<Contact> queryResult = ContactController.getContactsList(pagenumber, numberOfRecords, pageSize, searchString);
        Test.stopTest();
        System.assertEquals(1, queryResult.size());
    }
    @isTest
    static void testGetContactsCount(){
        
        String searchString = '';
        Test.startTest();
        Integer queryResult = ContactController.getContactsCount(searchString);
        Test.stopTest();
        System.assertEquals(11, queryResult);
        
    }

    @isTest
    static void testGetContactsCountJohnDoe(){
        
        String searchString = 'John';
        Test.startTest();
        Integer queryResult = ContactController.getContactsCount(searchString);
        Test.stopTest();
        System.assertEquals(1, queryResult);
    }
}